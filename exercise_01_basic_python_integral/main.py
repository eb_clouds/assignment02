import math


def integration(iterations, lower_border, upper_border):
    print(f"Number of iterations: {iterations}")
    print(f"Lower border of integral: {lower_border}")
    print(f"Upper border of integral: {upper_border}")
    dx = (upper_border - lower_border) / iterations
    pointer = 0
    integral = 0
    for i in range(iterations - 1):
        pointer += dx
        value = abs(math.sin(pointer))
        integral += value * dx
    print(f"Integral: {integral}")
    return integral


if __name__ == '__main__':
    Ns = [10, 100, 1000, 10000, 100000, 1000000]
    integralResult = {}
    for n in Ns:
        integralResult[str(n)] = integration(n, 0, 3.14159)
    print(f"{integralResult}")
