import math

from flask import Flask, request

app = Flask(__name__)


@app.route('/integration', methods=['GET'])
def integral_endpoint():
    print("Received new request")
    lower_border = request.args.get('lower', default = 1, type = float)
    upper_border = request.args.get('upper', default = 1, type = float)
    print(f"Lower border: {lower_border}")
    print(f"Upper border: {upper_border}")
    return execute_integration(lower_border, upper_border)


def execute_integration(lower_border, upper_border):
    Ns = [10, 100, 1000, 10000, 100000, 1000000]
    integral_result = {}
    for n in Ns:
        integral_result[str(n)] = integration(n, lower_border, upper_border)
    print(f"Result: {integral_result}")
    return integral_result


def integration(iterations, lower_border, upper_border):
    print(f"Number of iterations: {iterations}")
    print(f"Lower border of integral: {lower_border}")
    print(f"Upper border of integral: {upper_border}")
    dx = (upper_border - lower_border) / iterations
    pointer = 0
    integral = 0
    for i in range(iterations - 1):
        pointer += dx
        value = abs(math.sin(pointer))
        integral += value * dx
    print(f"Integral: {integral}")
    return integral


if __name__ == '__main__':
    app.run(port=8000, debug=True)
