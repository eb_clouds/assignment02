import logging
import json

import azure.functions as func
import azure.durable_functions as df



def orchestrator_function(context: df.DurableOrchestrationContext):

    files = yield context.call_activity('ReadData', "")
    
    reduced_files = {}
    for name, file in files.items():
        map_tasks = []
        for line in file:
            map_tasks.append(context.call_activity('Map', line))
        mapped_data_lines = yield context.task_all(map_tasks)
        combined_data = yield context.call_activity('Shuffle', mapped_data_lines)
        reduce_tasks = []
        for tuple in combined_data.items():
            reduce_tasks.append(context.call_activity('Reduce', tuple))
        reduced_data = yield context.task_all(reduce_tasks)
        reduced_files[name] = reduced_data
    return reduced_files

main = df.Orchestrator.create(orchestrator_function)
