# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(lines: list) -> dict:
    combined_pairs = {}
    for line in lines:
        for tuple in line:
            for key in tuple.keys():
                print(key)
                if key not in combined_pairs.keys():
                    combined_pairs[key] = [1]
                else:
                    combined_pairs[key].append(1)

    return combined_pairs
