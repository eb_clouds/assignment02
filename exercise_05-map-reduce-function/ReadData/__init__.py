import logging
import os, uuid
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient



def main(file: str):

    connect_str = "<>"
    files = {}
    try:
        print("Azure Blob Storage Python quickstart sample")
        blob_service_client = BlobServiceClient.from_connection_string(connect_str)


        print("\nListing blobs...")

        # List the blobs in the container
        container_client = blob_service_client.get_container_client("map-reduce-data")
        blob_list = container_client.list_blobs()
        for blob in blob_list:
            print("\t" + blob.name)
            data = container_client.download_blob(blob.name).readall().decode("utf-8")
            splitted_data = []
            counter = 0    
            for line in data.splitlines():
                print(line)
                splitted_data.append({counter: line})
                counter+=1
            files[blob.name] = splitted_data
    except Exception as ex:
        print('Exception:')
        print(ex)
    return files


