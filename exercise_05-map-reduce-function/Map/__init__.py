# Input: <key-value> pair
#       Key: line number
#       Value: content of the line

import logging
import json


def main(line: dict):
    result = []
    for line_value in line.values():
        words = line_value.split()
        for index in range(0,len(words)):
            word = words[index]
            result.append({word: 1})
    return result
